const express = require('express');
const path = require('path');
const logger = require('morgan');

const index = require('./routes/index');
var app = express();

// View engine setup
// No need to specify an engine because it's HTML
app.use(express.static(path.join(__dirname, 'public')));
// TODO: - Change HTML to pug; change folder name to views

// Middleware setup
app.use(logger('dev'));


// Routes
app.use('/', index);


module.exports = app;

/*
// =============================================================================
// HELPER FUNCTION
// =============================================================================

function dateNowReadable() {
  let myDate = new Date;
  return myDate.toLocaleString('en-US');
}

// =============================================================================
// SERVER
// =============================================================================

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
  console.log(`[${dateNowReadable()}] Connection established!`);
  socket.on('chat message', function(msg) {
    console.log(`[${dateNowReadable()}] MSG: ${msg}`);
    io.emit('chat message', msg);
  });
});

http.listen(3000, function() {
  console.log(`[${dateNowReadable()}] Now listening on *:3000`);
});

*/

// =============================================================================
// OLD, NON-FUNCTIONAL CODE
// =============================================================================

/*var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.set('port', (process.env.PORT || 3000));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.listen(app.get('port'), function(){
  console.log(`[${dateNowReadable()}] Now listening on *:${app.get('port')}`);
});
*/
