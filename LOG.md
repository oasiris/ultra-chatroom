# Purposes

This is a log I (David) am writing to myself to track my progress with this application.

-----

## Entry 3
## Posted 12/1/2017 @ 3:00 pm

Yesterday and today I only got a small amount of progress in because I've had other work -- in all four of my courses. There was a little bit of progress made.

- I watched a few minutes more of "Express Basics", which begins to delve into how to construct HTML pages out of multiple Pug templates using "partials" that pull from and extend each other.
- I worked a little more with passing in `res.local` data to Pug templates, but I'm excited to begin working on Angular where (maybe) a lot of this will go away.

-----

## Entry 2
### Posted 11/29/2017 @ 9:00 pm

Today, I only had a little over an hour to work on this. Here's what I did:

- I progressed in the "Express Basics" Team Treehouse course. I got a little under an hour of progress in and did the video's exercises while watching the videos. I left off on the video, "Using Logic in Pug."
    + In this hour, I learned the basics of Pug: that Pug is called a template engine, how to create views (which was surprisingly easy), and how to pass in variables using either the second parameter of `res.render` or setting the `res.locals` property/nested properties.
- I also moved the chatroom code into my `tt-express` Team Treehouse tutorial workspace folder. I converted my chatroom's HTML to Pug on via http://html2jade.org/, installed `socket.io` on `tt-express`, served it in my `app.js` file, and managed to get the chatroom file completely working when served and rendered via Express from Pug. (This was a lot easier than I thought it'd be.)
    + I also learned along the way that, if you put a JavaScript snippet in a Pug file a la the `script` tag, you _can't have indentation_ in the snippet or the Pug file doesn't render correctly into HTML. Sad.

I might have more time tomorrow. I also might have less time tomorrow. We'll see.

Friday, Saturday, and Sunday I will probably be forced to take a break from this project because I'll be in Yale for YHack.
 
-----

## Entry 1
### Posted 11/29/2017

Here's some of the stuff I've accomplished today and yesterday:

- I created this folder, ultra-chatroom.
- I followed the (quite poor) tutorial on how to create a chatroom on the Socket.IO website.
- I pushed this repository to Git and hosted it on Heroku (albeit unsuccessfully so far).
- I debugged my application! I learned the following from the (painful) debugging process. 
    + See Section 1 in "Learned" for details.
- I followed parts of a Team Treehouse tutorial for creating a Node.js application with no external libraries. 
    + See Section 2 in "Learned"
- I then moved on to the Team Treehouse tutorial for creating an Express application. It seems to have a total course time of 247 minutes. I didn't make it much through because I took a sidebar:
- I completed (from start to finish) the Team Treehouse course "HTTP Basics". As it mentioned, it walked me through the basics of HTTP protocol, and allowed me to interact with GET and POST requests via Telnet and the Chrome browser. Though much of it was review from what I had learned on my own during Wynd backend work, I did manage to learn some stuff about HTML forms, and I bookmarked a couple of pages for future useful readings
- Finally, I decided to **prioritize** this idea as something I definitely want to work on for a while coming. A chatroom is a great idea for learning MEAN; it's a fantastic project to show off to recruiters; and it's just going to be a lot of fun to be on the chatroom with my friends. I'm willing to put in hundreds of dollars a year to hosting a server for this.
    + Speaking of which -- I wonder if I could host this on the PI and just host this on Brown Champlin Wi-Fi year round... hmm.
    + This decision to make this project big is what lead me to spend a few hours on the following actions:
        * Polling friends for future features to implement
        * Creating a structured README.md and this LOG.md, so I can log my progress and have a clear direction where I want to go with this project!
        * Pushing this project to a public BitBucket repository (the GitHub variant is private for now because my GitHub profile is on resumes and stuff)
- I've probably spent a good 45-90 minutes on this particular entry in the log.

**I will attempt to spend at least an hour on this project every day.** Why? Because I want to set aside time to doing what's important to me every day. Learning and implementing software is important to me, so I should prioritize it in my life.

That's all for now. Good night!

###Learned

Here's some stuff I learned:

1.

- One must be careful if trying to start the server via Express. The tutorial uses http.Server because the `io` var (`var io = require('socket.io')(http)`) is gonna serve the client library `socket.io-client` to the HTML, and the only way it does that is by the server being hosted on HTTP.
    + Also, the `<script src="/socket.io/socket.io.js"></script>` is not a real file in the directory. Rather, it's served by the browser upon calling `http.listen` in `index.js`.

2.

- I learned a few things from this.
    + First, classes: I got a chance to work with JavaScript classes a little.
        * I got to see the old-skool JavaScript class, which can be constructed like this: 
        ```javascript
        function Profile(username) {
            EventEmitter.call(this);
        
            // ... code stuff
        }
        
        util.inherits( Profile, EventEmitter );
        ```

        * Let's compare and contrast that with the new class:

        ```javascript
        class Profile extends EventEmitter {
            constructor(username) {
                super();
                let profileEmitter = this;
                // ... code stuff
            }
        }
        ```

        * So this is super cool. Both of these `Profile` classes actually use `new Profile(<username>)` to be instantiated as objects.
    + There's something really cool going on in the Team Treehouse project's `example_profile.js` file. Take a look at the code for the file:
        * 
        ```javascript
        var Profile = require("./profile.js");

        var studentProfile = new Profile('chalkers');
        studentProfile.on("end", console.dir);
        studentProfile.on("error", console.error);
        ```
        * When the profile is instantiated, the block of code that fetches the data from the URL `"https://teamtreehouse.com/" + username + ".json"` is run. When the response has finally streamed all of the data, it'll emit the event `end` and pass the data as a string (parsed JSON body, to be precise). This `.on` method will notice that the event is fired and expose the data by passing it into the _callback_ function `console.dir`. This is super cool!
            - While we're on this page, `console.dir` is different from `console.log` in that the `dir` will almost always print out a navigable tree when run from web browser consoles such as Chrome or Firefox. It's like a more lengthy, metadata-friendly `log`.

Another thing I learned from the Team Treehouse:

- Everything's pretty easy until you add middleware.

I should still probably look over this to learn more about full-stack, though.

-----

_End of log_