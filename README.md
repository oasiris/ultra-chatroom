# Ol'Tra

^ That's the project codename for now. It's subject to change!

(Btw, what I've most recently been listening to: https://www.youtube.com/watch?v=1bZtCt_Siro)

Last README edit: 
11/29/2017

**See the /LOG.md file for nightly information!**

------

## Feature Brainstorming
`dis`
- Proposed by end-users (AKA "support group for life" high school friend chatroom) 11/28/2017
    + Profile pictures
    + Widgets
        * Drawing Pad
            - Pick colors
            - Draw with each other in real time
            - Put stickers on drawings
    + Uploading one's own emotes, a la Discord
    + BBCode Formatting
    + Markdown formatting
        * User configuration: which formatting to use
        * (pick: Markdown, BBCode, ...)
    + Name color changes
    + Ability to change your name in the first place
    + Emotican keyboard
        * One idea: a la TouchPal
        * "TouchPal had an option to switch to an emoticon keyboard, where all you had to do was "type" (press a button) and those series of characters would get typed into the message box"
    + Change size of text of a message using tags ("yell" vs "whisper") 
    + Easy to DM (click on name to DM)

- Proposed by me
    + /me command
    + User authentication using Mongo (or something cheaper maybe, lol)
    + User nicknames
    + 

- Not sure
    + How are we gonna structure these?
        * Channels? Guilds? Group chats? Threads? Boards?
        * 

- Wild/imaginative personal ideas
    + /me command
    + Ability for users to create and delete

## Masterlist planning

### Dependencies to include

This section is under construction!

### List of features to implement, in order

1.

    + Serve the pages using EJS or Pug
    + learn what these are called
    + (basically, learn MEAN Stack!!!)
    + Why?
        * because we want to be able to load NPM repositories and serve their data to the webpage, and that's impossible by serving just HTML because one would need to include necessary data as a query string or just import it manually which sucks

2.

    + A: Implement usernames. Assign each user a random username
    + B: Then, i.n.p.o (_in no particular order_):
        * ...

### Learning Goals

- Angular!
- All of MEAN, basically
- Event handlers, HTTP, server side logistics, IO, and whatnot
- ...

-----

_End of README_
David Hong, 2017
